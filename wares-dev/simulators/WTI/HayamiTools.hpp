/**
  @file HayamiTools.hpp
  @brief header of tools for Hayami propagation method
  @author Samir HOUASLI <samir.houasli@capgemini.com>
*/


#ifndef __HAYAMITOOLS_H__
#define __HAYAMITOOLS_H__


#include <vector>
#include <map>
#include <cmath>

#include <openfluid/core/TypeDefs.hpp>


typedef std::vector<double> t_HayamiKernel;
typedef std::map<int, t_HayamiKernel> IDKernelMap;


// =====================================================================
// =====================================================================


/**
  Computes the Hayami Kernel
  \param[in] Celerity Wave propagation (m/s)
  \param[in] Sigma Wave diffusivity (m2/s)
  \param[in] Length Transfer Length (m)
  \param[in] MaxSteps Maximum steps for the kernel length
  \param[in] TimeStep Time step duration (s)
  \param[out] Kernel Vector of double containing the kernel
*/
inline void ComputeHayamiKernel(double Celerity, double Sigma, double Length,
                                unsigned int MaxSteps, int TimeStep,
                                t_HayamiKernel& Kernel)
{
  float Theta, Zed;
  float Value1, Value2, Value3;
  float T;
  float Volume;

  unsigned int i;


  Kernel.clear();
  Kernel.resize(MaxSteps,0);

  Theta = Length / Celerity;
  Zed = (Celerity * Length) / ( 4 * Sigma);

  if (Zed < 0.5)
  {
    Zed = 0.5;
  }
  if (Zed > 50)
  {
    Zed = 50;
  }


  Value1 = pow((Theta * Zed / 3.1411592654),0.5);
  if (Theta > (0.1*TimeStep))
  {
    for (i=0;i<MaxSteps;i++)
    {
      T = ((i+1)-0.5) * TimeStep;
      Value2 = exp(Zed * (2 - (T/Theta) - (Theta/T)));
      Value3 = pow(T,1.5);
      Kernel[i] = Value1 * Value2 / Value3;
    }
  }
  else
  {
    Kernel[0] = 0.5 / TimeStep;
    Kernel[1] = 0.5 / TimeStep;
  }


  Volume = 0;


  for (i=0;i<Kernel.size();i++)
  {
    Volume = Volume + (Kernel[i] * TimeStep);
  }

  for (i=0;i<Kernel.size();i++)
  {
    Kernel[i] = Kernel[i] * (1/Volume);
  }
}


// =====================================================================
// =====================================================================


/**
  Propagates the wave using the Hayami kernel
*/
inline float DoHayamiPropagation(const t_HayamiKernel& Kernel,
                                 int CurrentStep, const openfluid::core::SerieOfDoubleValue* QInput,
                                 int MaxSteps, int TimeStep)
{
  float QOutput;
  int ZeEnd;


  ZeEnd = MaxSteps;
  if (CurrentStep < ZeEnd)
  {
    ZeEnd = CurrentStep;
  }

  QOutput = 0;

  for (int i=0;i<ZeEnd;i++)
  {
    QOutput = QOutput + (Kernel[i] * QInput->at(CurrentStep - i) * TimeStep);
  }


  return QOutput;
}

#endif // __HAYAMITOOLS_H__