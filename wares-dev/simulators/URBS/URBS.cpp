/**
  @file URBS.cpp
*/


/*
<sim2doc>

</sim2doc>
*/


#include <openfluid/ware/PluggableSimulator.hpp>
#include <openfluid/tools/DataHelpers.hpp>
#include <openfluid/scientific/FloatingPoint.hpp>
#include <algorithm>
#include <cmath>
// =====================================================================
// =====================================================================


BEGIN_SIMULATOR_SIGNATURE("URBS")

  // Informations
  DECLARE_NAME("URBS")
  DECLARE_DESCRIPTION("Urban Cadastral Units")
  DECLARE_VERSION("1.1")
  DECLARE_AUTHOR("Kristi Z.","kristi.zoto@capgemini.com");
  DECLARE_STATUS(openfluid::ware::STABLE)
  DECLARE_VERSION("1.1");
  
  DECLARE_DOMAIN("hydrology");
  DECLARE_PROCESS("Urban cadastral interaction");
  DECLARE_METHOD("");
  
  // Produced variables
  DECLARE_PRODUCED_VARIABLE("s.tree","SU","the water height intercepted(stored) by trees","m");
  DECLARE_PRODUCED_VARIABLE("s.surf","SU","the water height intercepted(stored) by the surface","m");
  DECLARE_PRODUCED_VARIABLE("z.sat","SU","average saturation level of each surface type weighted by the land use surface proportion","m");
  DECLARE_PRODUCED_VARIABLE("i.drain","SU","flux of water from the saturated zone into the dranaige zone","m/s");
  // Required and Used variables
  DECLARE_REQUIRED_VARIABLE("water.atm-surf.H.rain","SU","rainfall height on the SU","m");/*from INPUT module*/

  DECLARE_USED_VARIABLE("PET","SU","Penman-Monteith potential evapotranspiration","m/s");/*from Generator module*/  
  
  // Required and used attributes
  //tree zone
  DECLARE_USED_ATTRIBUTE("streemin","SU","minimum value of the tree interception reservoir","m");
  DECLARE_USED_ATTRIBUTE("a","SU","drainage law parameter","s(-1)");
  DECLARE_USED_ATTRIBUTE("ftree","SU","fraction of surface area covered by trees","-");
  
  //surface zone
  DECLARE_USED_ATTRIBUTE("Ks","SU","hydraulic conductivity at ground level saturation","m/s");
  DECLARE_USED_ATTRIBUTE("ssurfmax","SU","maximum storage capacity of the surface reservoir","m");
  
  //saturated zone
  DECLARE_USED_ATTRIBUTE("znet","SU","depth of the rainwater drainage network","m");
  DECLARE_USED_ATTRIBUTE("zsoil","SU","altitude of the UHE","m");
  DECLARE_USED_ATTRIBUTE("lambda","SU","parameter of the type and state of the drain and trench containing the drain(groundwater drainage coefficient)","-");
  DECLARE_USED_ATTRIBUTE("mi","SU","parameter of the type and state of the drain and trench containing the drain(groundwater drainage exponent)","-");
  DECLARE_USED_ATTRIBUTE("L","SU","length of the UHE","m");
  DECLARE_USED_ATTRIBUTE("M","SU","Scaling parameter of the hydraulic conductivity","-");
  DECLARE_REQUIRED_ATTRIBUTE("thetasat","SU","saturated soil water content","m3/m3");
  DECLARE_SCHEDULING_DEFAULT;

END_SIMULATOR_SIGNATURE


// =====================================================================
// =====================================================================


/**

*/
class URBS : public openfluid::ware::PluggableSimulator
{
  private:
      
  public:

  
    URBS(): PluggableSimulator()
    {
  
  
    }
  
  
    // =====================================================================
    // =====================================================================
  
  
    ~URBS()
    {
  
  
    }
  
  
    // =====================================================================
    // =====================================================================
  
  
    void initParams(const openfluid::ware::WareParams_t& /*Params*/)
    {


    }


    // =====================================================================
    // =====================================================================
  
  
    void prepareData()
    {
  
  
    }
  
  
    // =====================================================================
    // =====================================================================
  
  
    void checkConsistency()
    {
      
      openfluid::core::SpatialUnit* SU;
      openfluid::core::UnitID_t ID;
      openfluid::core::DoubleValue Streemin,A,Ftree,Ks,Ssurfmax;
      openfluid::core::DoubleValue Znet,Zsoil,Lambda,Mi,L,M,ThetaS; 
      
      OPENFLUID_UNITS_ORDERED_LOOP("SU",SU)
      {
        ID = SU->getID();
      
        // Getting spatial attributes for tree zone
        OPENFLUID_GetAttribute(SU,"streemin",Streemin);
        OPENFLUID_GetAttribute(SU,"a",A);
        OPENFLUID_GetAttribute(SU,"ftree",Ftree);
        
        //Getting spatial attributes for surface zone
        OPENFLUID_GetAttribute(SU,"Ks",Ks);
        OPENFLUID_GetAttribute(SU,"ssurfmax",Ssurfmax);
        
        //Getting spatial attributes for saturated zone
        OPENFLUID_GetAttribute(SU,"znet",Znet);
        OPENFLUID_GetAttribute(SU,"zsoil",Zsoil);
        OPENFLUID_GetAttribute(SU,"lambda",Lambda);
        OPENFLUID_GetAttribute(SU,"mi",Mi);
        OPENFLUID_GetAttribute(SU,"L",L);
        OPENFLUID_GetAttribute(SU,"M",M);
        OPENFLUID_GetAttribute(SU,"thetasat",ThetaS);
        
        std::string IDStr;
        openfluid::tools::convertValue(ID,&IDStr);
        
        if (Streemin <= 0)
        {
          OPENFLUID_RaiseError("The stremin of the SU " + IDStr + " is negative or null.");
        }
        if (A <= 0)
        {
          OPENFLUID_RaiseError("The a of the SU " + IDStr + " is negative or null.");
        }
        if (Ftree <= 0)
        {
          OPENFLUID_RaiseError("The ftree of the SU " + IDStr + " is negative or null.");
        }
        if (Ks <= 0)
        {
          OPENFLUID_RaiseError("The Ks of the SU " + IDStr + " is negative or null.");
        }        
        if (Ssurfmax <= 0)
        {
          OPENFLUID_RaiseError("The ssurfmax of the SU " + IDStr + " is negative or null.");
        }
        if (Znet <= 0)
        {
          OPENFLUID_RaiseError("The znet of the SU " + IDStr + " is negative or null.");
        }
        if (Zsoil <= 0)
        {
          OPENFLUID_RaiseError("The zsoil of the SU " + IDStr + " is negative or null.");
        }
        if (Lambda <= 0)
        {
          OPENFLUID_RaiseError("The lambda of the SU " + IDStr + " is negative or null.");
        }
        if (Mi <= 0)
        {
          OPENFLUID_RaiseError("The mi of the SU " + IDStr + " is negative or null.");
        }
        if (L <= 0)
        {
          OPENFLUID_RaiseError("The L of the SU " + IDStr + " is negative or null.");
        }
        if (M <= 0)
        {
          OPENFLUID_RaiseError("The M of the SU " + IDStr + " is negative or null.");
        }
        if (ThetaS <= 0)
        {
          OPENFLUID_RaiseError("The thetasat of the SU " + IDStr + " is negative or null.");
        }
      }
  
    }
  
  
    // =====================================================================
    // =====================================================================
  
  
    openfluid::base::SchedulingRequest initializeRun()
    { 
      openfluid::core::SpatialUnit* SU;
      OPENFLUID_UNITS_ORDERED_LOOP("SU",SU)
      {
        
        OPENFLUID_InitializeVariable(SU,"s.tree",0.0);
        OPENFLUID_InitializeVariable(SU,"s.surf",0.0);
        OPENFLUID_InitializeVariable(SU,"z.sat",0.3);
        OPENFLUID_InitializeVariable(SU,"i.drain",0.0);
      }
      return DefaultDeltaT();
    }


    // =====================================================================
    // =====================================================================
  
  
    openfluid::base::SchedulingRequest runStep()
    {
      int ID;
      double RainIntensity;
      unsigned int CurrentStep;
      unsigned int TimeStep;
      
      
      double Etree;
      double Otree;
      double Stree; 
      double auxiliary;
      
      double Esurf;
      double Ssurf;
      double infiltration;
      double runoff;
      
      openfluid::core::DoubleValue Streemin;
      openfluid::core::DoubleValue A;
      openfluid::core::DoubleValue Ssurfmax;
      openfluid::core::DoubleValue Ftree;
      openfluid::core::DoubleValue Ks;
      openfluid::core::DoubleValue Znet;
      openfluid::core::DoubleValue Zsoil;
      openfluid::core::DoubleValue Lambda;
      openfluid::core::DoubleValue Mi;
      openfluid::core::DoubleValue L;
      openfluid::core::DoubleValue M;
      openfluid::core::DoubleValue ThetaS;
      
      openfluid::core::DoubleValue CurrentRain;
      openfluid::core::DoubleValue CurrentPET;
      openfluid::core::DoubleValue TmpValueStreePrevious;
      openfluid::core::DoubleValue TmpValueSsurfPrevious;
      openfluid::core::DoubleValue TmpValueZsatPrevious;
      
      openfluid::core::SpatialUnit* SU;
      
      TimeStep = OPENFLUID_GetDefaultDeltaT();
      openfluid::core::TimeIndex_t CurrentTimeIndex = OPENFLUID_GetCurrentTimeIndex();
      CurrentStep = CurrentTimeIndex / TimeStep;
      
      openfluid::core::TimeIndex_t PreviousTimeIndex = OPENFLUID_GetPreviousRunTimeIndex();
      
      OPENFLUID_UNITS_ORDERED_LOOP("SU",SU)
      {
        ID = SU->getID();
        OPENFLUID_GetAttribute(SU,"streemin",Streemin);
        OPENFLUID_GetAttribute(SU,"a",A);
        OPENFLUID_GetAttribute(SU,"ssurfmax",Ssurfmax);
        OPENFLUID_GetAttribute(SU,"ftree",Ftree);
        OPENFLUID_GetAttribute(SU,"Ks",Ks);
        
        Etree=0;
        Otree=0;
        infiltration=0;
        runoff = 0;
        Esurf=0;

        // Convert rain from m/s to m/time step
        OPENFLUID_GetVariable(SU,"water.atm-surf.H.rain",CurrentRain);
        
        // Computing rain intensity : P(t)
        //we keep the metres/step logic
        RainIntensity = CurrentRain;

        //Compute Etree(t),Otree(t),Stree(t)
        if (OPENFLUID_IsVariableExist(SU,"s.tree",PreviousTimeIndex),
                                            openfluid::core::Value::DOUBLE)
        {
          OPENFLUID_GetVariable(SU,"s.tree",PreviousTimeIndex,TmpValueStreePrevious);
          if(TmpValueStreePrevious<=Streemin){
            if(OPENFLUID_IsVariableExist(SU,"PET")){
              OPENFLUID_GetVariable(SU,"PET",CurrentPET);
              Etree = (TmpValueStreePrevious/Streemin)*(CurrentPET/1000);
              Otree = 0.0; 
            }
          }
          else{
            if(OPENFLUID_IsVariableExist(SU,"PET")){
              OPENFLUID_GetVariable(SU,"PET",CurrentPET);
              Etree = (CurrentPET/1000);
              
              auxiliary = (double)floor(abs(TmpValueStreePrevious.get()-Streemin.get()));
              Otree=(A/60)*auxiliary; 
            }
          }
          double  previousValue = (double)TmpValueStreePrevious.get();
          Stree = previousValue + (RainIntensity-Etree-Otree);
                        
          OPENFLUID_AppendVariable(SU,"s.tree",Stree);
        }
        
        //Compute Ssurf(t),Esurf(t),I(t),R(t)
        
        if(OPENFLUID_IsVariableExist(SU,"s.surf",PreviousTimeIndex),
                                            openfluid::core::Value::DOUBLE)
        {
           
           OPENFLUID_GetVariable(SU,"s.surf",PreviousTimeIndex,TmpValueSsurfPrevious);
        
           if(OPENFLUID_IsVariableExist(SU,"PET")){
             OPENFLUID_GetVariable(SU,"PET",CurrentPET);
             Esurf = (TmpValueSsurfPrevious/Ssurfmax)*(CurrentPET/1000);
           }
           infiltration = (double)std::min(Ks.get(),(double)TmpValueSsurfPrevious);
           double difference = (TmpValueSsurfPrevious.get() - Ssurfmax.get());
           //double ratio = (double)(difference);
           double zero = 0.0;
           runoff = (double)std::max(zero,difference);
           double ftree = (double)Ftree.get();            
           auxiliary = (double)((((1-ftree)*RainIntensity)+(ftree*Otree)-Esurf-infiltration-runoff));
           double  previousValue = (double)TmpValueSsurfPrevious.get();
           
           Ssurf= previousValue + auxiliary;
           OPENFLUID_AppendVariable(SU,"s.surf",Ssurf);
        }
        
        //Compute zs(t),Idrain(t)
        if(OPENFLUID_IsVariableExist(SU,"z.sat",PreviousTimeIndex),
                                            openfluid::core::Value::DOUBLE){
          
             OPENFLUID_GetVariable(SU,"z.sat",PreviousTimeIndex,TmpValueZsatPrevious);
             OPENFLUID_GetAttribute(SU,"M",M);
             OPENFLUID_GetAttribute(SU,"L",L);
             OPENFLUID_GetAttribute(SU,"lambda",Lambda);
             OPENFLUID_GetAttribute(SU,"zsoil",Zsoil);
             OPENFLUID_GetAttribute(SU,"znet",Znet);
             OPENFLUID_GetAttribute(SU,"mi",Mi);
             double power = ((-1)*TmpValueZsatPrevious)/M;
             const double EulerConstantPower = std::exp(power);
             double ratioCoefficient;
             if(L!=0){
               ratioCoefficient = Lambda/L;
             }
             double difference = Zsoil.get()-Znet.get()-TmpValueZsatPrevious.get();
             double differencePower = std::pow(difference,Mi.get()); 
             double Idrain = Ks * EulerConstantPower * ratioCoefficient * differencePower;
              
             OPENFLUID_AppendVariable(SU,"i.drain",Idrain);
           
        } 
        if(OPENFLUID_IsVariableExist(SU,"i.drain")){
             openfluid::core::DoubleValue Idrain;
             OPENFLUID_GetVariable(SU,"i.drain",Idrain);
             OPENFLUID_GetAttribute(SU,"thetasat",ThetaS);
             double  previousValue = (double)TmpValueZsatPrevious.get();
             double Zsat = previousValue + (Idrain/ThetaS);
             OPENFLUID_AppendVariable(SU,"z.sat",Zsat);
        }
       }
      return DefaultDeltaT();
    }


    // =====================================================================
    // =====================================================================
  
  
    void finalizeRun()
    {
  
  
    }

};


// =====================================================================
// =====================================================================


DEFINE_SIMULATOR_CLASS(URBS);


DEFINE_WARE_LINKUID(WARE_LINKUID)